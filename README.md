# jolly JSON

Would you like to extend JSON with encryption, verification for a trustable infrastructure?
Minify and optimize the adaptive JSON format & try use options (environments, spaces, etc.).
Check out "jolly JSON" and integrate it into your next project.